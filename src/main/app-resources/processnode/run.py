#!/opt/anaconda/bin/python
#node to use the mask output of node A to execute the rest of the workflow
#============================IMPORTS===========================================
#standard
import sys
import os
import subprocess
import time
import zipfile
import fnmatch
import atexit
import tarfile
import ftplib

#common
import ftputil
import geojson
import utils as uutt
import urllib

#unknown
import shutil
from lxml import etree

# import the ciop functions (e.g. copy, log)
import cioppy 
ciop = cioppy.Cioppy()

#==============================================================================

#Fetching all args from application.xml
startdate   = ciop.getparam("startdate") 	#format: "2010-01-01"
enddate     = ciop.getparam("enddate")		#format: "2016-01-01"
minpoints   = '1'				#format: 1
maxstderror = '1.5'				#format: 0.1
plotname    = ciop.getparam("plotname") 	#format: "kainji"
bbox	    = ciop.getparam("bbox")			#format: 4.0,10.0,5.1,11.0
missions    = ciop.getparam("missions")		#format: "JS2,ATK"

uutt.checkALLformats(startdate,enddate,bbox,missions)

startdateformat = startdate
enddateformat = enddate
missions = uutt.reformat_missions( missions )
plotname = plotname.replace(' ','_')
startdate = startdate.replace('-','')
enddate = enddate.replace('-','')
bbox = uutt.reformat_bbox( bbox )

#==============================================================================

#setting up pathing
tmpdir = ciop.tmp_dir
appdir = ciop.application_dir

levelpath = os.path.join(appdir, "processnode")
resourcepath = os.path.join(levelpath, "resources")
environment = "/opt/MCR_R2016b/v91"
dirlist = []
zippath = os.path.join(tmpdir, "zip")
dirlist.append(zippath)
outpath = os.path.join(tmpdir, "outputs")
dirlist.append(outpath)
inpath = os.path.join(tmpdir, "inputs")
dirlist.append(inpath)
maskcalc = os.path.join(tmpdir, "maskcalc")
dirlist.append(maskcalc)
datapath = os.path.join(inpath, "data")
dirlist.append(datapath)
satlist = ["ATK","SN3","JS2"]
for item in satlist:
    satpath = os.path.join(datapath,item)
    dirlist.append(satpath)
    dirlist.append(os.path.join(satpath,"L2"))
    dirlist.append(os.path.join(satpath,"tmp"))
for item in dirlist:
    os.mkdir(item)

downloadedlist = []
index = 0

for inputitem in sys.stdin:
	ciop.log('INFO','input file is:'+inputitem)
	maskpath = ciop.copy(inputitem,maskcalc)
	maskname = os.path.basename(maskpath).replace('.txt','').replace('.tif','')
	indexplotname = plotname + '_' + maskname
	time1 = time.time()

	#==============================================================================================================================================
	#obtaining all track numbers that lie within the bounding box
	bbtolerance = 0.25
	#trackdetailstr = tepfunctions.fetchtracks(bbox,bbtolerance,missions)
	searchbox = bbox
	searchbox = str.split(searchbox,',')
	searchbox = [float(i) for i in searchbox]
	searchbox[0] = min(searchbox[0::2]) - bbtolerance
	searchbox[1] = min(searchbox[1::2]) - bbtolerance
	searchbox[2] = max(searchbox[0::2]) + bbtolerance
	searchbox[3] = max(searchbox[1::2]) + bbtolerance
	searchbox = [str(i) for i in searchbox]
	searchbox = searchbox[0]+','+searchbox[1]+','+searchbox[2]+','+searchbox[3]

	trackdetailstr = list()

	for mission in missions:
		if mission == "JS2":
			source = 'https://catalog.terradue.com/jason2/series/GPS/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","JS2","L2")
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
					
		if mission == "ATK":
			source = 'https://catalog.terradue.com/saral/series/GPS/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","ATK","L2")
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
	
		if mission == "SN3":
			source = 'https://catalog.terradue.com/sentinel3/series/SR_2_LAN/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","SN3","L2")
			#command = ['opensearch-client','-m','EOP','-p','count=100','-p','bbox='+searchbox,source,'track,enclosure']
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
		
		uutt.cioplog(' 1D query command ',command)
		trackcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
		tracks = trackcatch.stdout.read().rstrip()
		tracks = str.split(tracks,'\n')
		
		if mission =="SN3":
			enclosures_list = uutt.select_by_priority( [str.split(item,',')[1] for item in tracks] )
			tracks = [uutt.tracknofroms3url( single_enclosure )+','+single_enclosure for single_enclosure in enclosures_list]
			#ciop.log('INFO','mission=S3, ' + str(['cycle='+uutt.cylcefroms3url( se )+'trackno='+uutt.tracknofroms3url( se ) for se in enclosures_list]) )
			ciop.log('INFO','mission='+mission+', cycle=020'+', track numbers are: ' + str([str.split(item,',')[0] for item in tracks]) )
		else:
			ciop.log('INFO','mission='+mission+', cycle=020'+', track numbers are: ' + str([str.split(item,',')[0] for item in tracks]) )
		
		for item in tracks:
			try:
				trackno,enclosure = str.split(item,',')
				#trackfile = ciop.copy(enclosure,maskcalc,extract=True)
				trackfile = ciop.copy(enclosure,target,extract=True)
				if mission == "SN3":
					SN3name = os.listdir(trackfile)[0]
					src = os.path.join(trackfile,SN3name)
					#dest = os.path.join(maskcalc,SN3name)
					dest = os.path.join(target,SN3name)
					os.rename(src,dest)
					shutil.rmtree(trackfile)
					trackfile = dest
				if os.path.exists(trackfile):
					trackdetailstr = trackdetailstr + [mission+':'+trackno+':'+trackfile]
					downloadedlist = downloadedlist + [enclosure] 
			except:
				print('No tracks found for '+mission)

	if trackdetailstr == []:
		ciop.log("ERROR"," No tracks found ")
		sys.exit(1)
	#==============================================================================================================================================

	time2 = time.time()

	#MASKSCRIPT
	grouppath = os.path.join(tmpdir,indexplotname+"_group.txt")

	envstring = ".:"+environment+"/runtime/glnxa64:"+environment+"/bin/glnxa64:"+environment+"/sys/os/glnxa64:"+environment+"/sys/opengl/lib/glnxa64"

	os.environ["LD_LIBRARY_PATH"] = envstring
	command = os.path.join(resourcepath, "run_maskscript")

	err1_out1_list = ['<inner_processor1_messages>'+"\n \n"]
	#if maskpath.endswith('.tif'):
	#	item = ','.join(trackdetailstr)
	#	uutt.cioplog(' 1M command ',[command,grouppath,maskpath,item,bbox])
	#	fullcatch1 = subprocess.Popen([command,grouppath,maskpath,item,bbox],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
	#	errorcatch1 = fullcatch1.stderr.read().rstrip()
	#	outcatch1 = fullcatch1.stdout.read().rstrip()
	#	err1_out1_list.append(str(errorcatch1)+"\n\n"+str(outcatch1)+"\n\n")
	#else:
	#	for item in trackdetailstr:
	#		uutt.cioplog(' 1M command ',[command,grouppath,maskpath,item,bbox])
	#		fullcatch1 = subprocess.Popen([command,grouppath,maskpath,item,bbox],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
	#		errorcatch1 = fullcatch1.stderr.read().rstrip()
	#		outcatch1 = fullcatch1.stdout.read().rstrip()
	#		err1_out1_list.append(str(errorcatch1)+"\n\n"+str(outcatch1)+"\n\n")
	for item in trackdetailstr:
		uutt.cioplog(' 1M command ',[command,grouppath,maskpath,item,bbox])
		fullcatch1 = subprocess.Popen([command,grouppath,maskpath,item,bbox],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
		errorcatch1 = fullcatch1.stderr.read().rstrip()
		outcatch1 = fullcatch1.stdout.read().rstrip()
		err1_out1_list.append(str(errorcatch1)+"\n\n"+str(outcatch1)+" \n\n")
	err1_out1_list.append('</inner_processor1_messages>'+" \n \n")


	#==============================================================================================================================================

	time3 = time.time()

	#obtaining altimetry data
	#get missionpasslist from group.txt

	grouppaths = [os.path.join(tmpdir,ff) for ff in os.listdir( tmpdir ) if 'group' in ff and ff.endswith('.txt')]
	for item in grouppaths:
		ciop.log('INFO','Items in grouppaths:'+ str(item))
	grouplines = []
	files_opbtained_list = []
	for grouppath_el in grouppaths:
		groupfile = open(grouppath_el,'r')
		grouplist = groupfile.read().rstrip()
		grouplines = grouplist.split('\n')
		grouplist = [i.split(' ') for i in grouplines]

		missionpasslist = list()
		try:
			for item in grouplist:
				missionpasslist = missionpasslist + [item[0]+':'+item[1]]
		except:
			print('No tracks cross waterbody')
			ciop.log("ERROR"," No samples into waterbody ")
			continue
			# we want to continue over the other water ids

		#missionpasslist = str.split(missionpasses,",")
		startvar = 'time:start='+str(startdateformat)
		endvar = 'time:end='+str(enddateformat)
		#files_opbtained = open(os.path.join(outpath,"filelog.txt"),'a')

		for item in missionpasslist:
		
			sat, trackno = str.split(item,':')
			trackvar = 'track='+str(trackno)

			if sat == "JS2":
				target = os.path.join(tmpdir,"inputs","data","JS2","L2")
				source = 'https://catalog.terradue.com/jason2/series/GPS/search?format=atom'
				command = ['opensearch-client','-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']
						
			if sat == "ATK":
				target = os.path.join(tmpdir,"inputs","data","ATK","L2")
				source = 'https://catalog.terradue.com/saral/series/GPS/search?format=atom'
				command = ['opensearch-client','-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']

			if sat == "SN3":
				target = os.path.join(tmpdir,"inputs","data","SN3","tmp")
				SN3target = os.path.join(tmpdir,"inputs","data","SN3","L2")
				source = 'https://catalog.terradue.com/sentinel3/series/SR_2_LAN/search?format=atom'
				command = ['opensearch-client','-p','bbox='+searchbox,'-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']
		

			uutt.cioplog(' 2D query command ',command)
			urlcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
			urllist = urlcatch.stdout.read().rstrip()
			urllist = urllist.split('\n')
			urllist = list(set(urllist) - set(downloadedlist))

			if not(urllist == '') and not(urllist ==[]) and not(urllist ==['']):
				if sat=='SN3':
					urllist = uutt.select_by_priority(urllist)
				for url in urllist:
					try:
						if sat == "SN3":
							#if '_NT_' in url:
							filestr = ciop.copy(url,target) #filestr will be the latest altimetry track downloaded
							SN3name = os.listdir(filestr)[0]
							src = os.path.join(filestr,SN3name)
							dest = os.path.join(SN3target,SN3name)
							os.rename(src,dest)
							shutil.rmtree(filestr)
							filestr = dest
						else:
							filestr = ciop.copy(url,target) #filestr will be the latest altimetry track downloaded
						#files_opbtained.write(str(url)+"\n")
						#files_opbtained.write(str(filestr)+"\n")
						files_opbtained_list.append( str(url)+"\n"+str(filestr)+"\n" )
						downloadedlist = downloadedlist + [url]
					except:
						print('error copying file')

			#		tounzip = os.path.join(zippath, filestr)
			#		with zipfile.ZipFile(tounzip,'r') as zip_ref:
			#			filestr = zip_ref.namelist()(0)
			#			zip_ref.extractall(target)

		#files_opbtained.close()

	#==============================================================================================================================================

	#LAKE_LEVEL_SCRIPT

	time4 = time.time()
	err2_out2_list = ['<inner_processor2_messages>'+"\n \n"]
	#errorcatch2 = "blank mtlb error"
	#outcatch2 = "blank 2M stdout"

	for grouppath_el in grouppaths:
		indexplotname_el = os.path.split(grouppath_el)[1].replace('.txt','').replace('_group','')
		command = os.path.join(resourcepath, "run_lake_level_wrapper")
		#multiplot = '1'
		uutt.cioplog(' 2M command ',[command,tmpdir+"/",grouppath_el,startdate,enddate,minpoints,maxstderror,indexplotname_el])
		fullcatch = subprocess.Popen([command,tmpdir+"/",grouppath_el,startdate,enddate,minpoints,maxstderror,indexplotname_el],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
		errorcatch2 = fullcatch.stderr.read().rstrip()
		outcatch2 = fullcatch.stdout.read().rstrip()
		err2_out2_list.append(str(errorcatch2)+"\n\n"+str(outcatch2)+" \n\n")
	err2_out2_list.append('</inner_processor2_messages>'+" \n \n")

	#==============================================================================================================================================

	time5 = time.time()
	
	down1time = time2-time1
	matlab1time = time3-time2
	down2time = time4-time3
	matlab2time = time5-time4

	file_object = open(os.path.join(outpath,indexplotname+"_log.txt"),'w')
	time_msg = "Download time: "+str(down1time)+"\nMatlab part 1 time: "+str(matlab1time)+"\nDownload time2: "+str(down2time)+"\nMatlab part 2 time: "+str(matlab2time)+"\n"
	file_object.write("\n"+'<time_log>'+" \n\n" + time_msg + "\n"+'</time_log>'+" \n\n")
	file_object.write("\n"+'<altimetry_files>'+" \n")
	for item in files_opbtained_list:
		file_object.write(item)
	file_object.write("\n"+'</altimetry_files>'+" \n\n")
	for item in err1_out1_list:
		file_object.write(item.replace("\n\n\n\n",'\n').replace("\n\n\n",'\n').replace("\n\n",'\n'))
	for item in err2_out2_list:
		file_object.write(item.replace("\n\n\n\n",'\n').replace("\n\n\n",'\n').replace("\n\n",'\n'))
	file_object.write("\n"+'<track_details>'+" \n\n")
	for item in trackdetailstr:
		file_object.write(str(item)+'\n')
	file_object.write("\n"+'</track_details>'+" \n\n")
	file_object.close()

	#foreach grouppath
	for grouppath_el in grouppaths:
		# read, edit SN3, copy2dest
		f = open(grouppath_el,'r')
		content = f.read()
		file_object.close()
		group_dest = os.path.join( outpath , os.path.basename(grouppath_el).replace('group','details') )
		f = open(group_dest,'w')
		f.write(content.replace('SN3','S3'))
		f.close()
		#group_dest = os.path.join( outpath , os.path.basename(grouppath_el).replace('group','details') )
		#os.rename( grouppath_el , group_dest )
	geotiffpaths = [os.path.join(tmpdir,ff) for ff in os.listdir( tmpdir ) if 'waterIDs' in ff and ff.endswith('.tif')]
	for geotiffpath in geotiffpaths:
		geotiffpath_dest = os.path.join( outpath , os.path.basename(geotiffpath).replace(maskname,indexplotname) )
		os.rename( geotiffpath , geotiffpath_dest )
	#file_group = open(os.path.join(outpath,indexplotname+"_group.txt"),'w')
	#for item in grouplines:
	#	file_group.write(str(item)+"\n")
	#file_group.close()

	#experiment: generate a world file for the jpgs
	#filelist = []
	#dirlist = os.listdir(outpath)
	#for item in dirlist:
	#	if os.path.isfile(os.path.join(outpath,item)):
	#		filelist.append(os.path.join(outpath,item))
	#
	#for item in filelist:
	#	if item.endswith('.jpg'):
	#		worldfile = open(item+"w",'w')
	#		worldfile.write('0.06 \n 0.0 \n 0.0 \n -0.01 \n -170.0 \n 80.0')
	#		worldfile.close()

	#zip the jpgs
	filelist = [os.path.join(outpath,item) for item in os.listdir(outpath) if os.path.isfile(os.path.join(outpath,item))]
	for item in filelist:
		if item.endswith('.jpg'):
			zf = zipfile.ZipFile(item.replace('.jpg','')+'.zip',mode='w')
			try:
				zf.write(item,os.path.basename(item))
			finally:
				zf.close()
				os.remove(item)
		elif item.endswith('.csv'):
			f = open(item,'r')
			if len(f.readlines())<2:
				f.close()
				os.remove(item)
			else:
				f.close()
			
new_filelist = [os.path.join(outpath,item) for item in os.listdir(outpath) if os.path.isfile(os.path.join(outpath,item))]
for fullfilename in new_filelist:
	ciop.publish(fullfilename,metalink=True)
#note that the publish function publishes the outputs for all lakes and thus it needs to be outside of the loop
#ciop.publish(outpath,recursive=True,metalink=True)

sys.exit(0)

