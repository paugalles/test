import os
import cioppy 
import sys
ciop = cioppy.Cioppy()


def checkformat(input_arg,condition,msg):
	err_code_format = 100
	if condition=='date':
		iswrong = len(input_arg)!=10
		if not iswrong:
			iswrong = not all([input_arg[i].isdigit() for i in [0,1,2,3,5,6,8,9]]) or not all([input_arg[i]=="-" for i in [4,7]])
		err_code_format = err_code_format+1
	elif condition=='bbox':
		iswrong = not all([el in '-0123456789,POLYGON() .' for el in input_arg])
		err_code_format = err_code_format+2
	elif condition=='missions':
		iswrong = not all([el in 'S3JNATK2,L' for el in input_arg])
		err_code_format = err_code_format+3
	if iswrong:
		ciop.log("ERROR",msg)
		sys.exit(err_code_format)

def checkALLformats(startdate,enddate,bbox,missions):
	checkformat(startdate,'date',"start date format is wrong, must be yyyy-mm-dd")
	checkformat(enddate,'date',"end date format is wrong, must be yyyy-mm-dd")
	checkformat(bbox,'bbox',"input box format is wrong ")
	checkformat(missions,'missions',"wrong missions format, valid formats are S3,JS,ATK they must be comma separated")

def cioplog(infostr,contentstr):
	ciop.log('INFO','______________________________'+infostr+'______________________________')
	ciop.log('INFO',contentstr)
	ciop.log('INFO','______________________________end of '+infostr+'______________________________')

def bbox2array( bbox ):
	return bbox.replace('POLYGON((','').replace('))','').replace(' ',',')

def reformat_missions( missions ):
	missions = missions.replace('ALL','ATK,JS2,SN3')
	missions = missions.replace('a','A')
	missions = missions.replace('t','T')
	missions = missions.replace('k','K')
	missions = missions.replace('s','S')
	missions = missions.replace('n','N')
	missions = missions.replace('j','J')
	missions = missions.replace('S3','SN3')
	missions = missions.replace('JS2','JS') #order dependant
	missions = missions.replace('JS','JS2')
	missions = missions.replace('JN2','JS2')
	missions = missions.replace('JN','JS2')
	missions = str.split(missions,',')
	return missions

def reformat_plotname( plotname ):
	return plotname.replace(' ','_')

def reformat_mask( mask ):
	return mask.replace('&amp;','&')

def reformat_startdate( startdate ):
	return startdate.replace('-','')

def reformat_enddate( enddate ):
	return enddate.replace('-','')

def reformat_bbox( bbox ):
	#TODO non squared polygon
	if ' ' in bbox and len(bbox.split())==4:
		bbox=','.join([ bbox.split()[i] for i in [1,0,3,2] ])
	elif 'POLYGON((' in bbox:
		bbox = bbox2array( bbox )
	return bbox

def reformat_all(startdate,enddate,plotname,bbox,mask,missions):
	missions = reformat_missions( missions )
	plotname = reformat_plotname( plotname )
	mask = reformat_mask( mask )
	startdate = reformat_startdate( startdate )
	enddate = reformat_enddate( enddate )
	bbox = reformat_bbox( bbox )
	return startdate,enddate,plotname,bbox,mask,missions
#startdate,enddate,plotname,bbox,mask,missions = reformat_all(startdate,enddate,plotname,bbox,mask,missions)


def build_levelpath(appdir): return os.path.join(appdir, "level")

def build_resourcepath(levelpath): return os.path.join(levelpath, "resources")

def build_environment(): return "/opt/MCR_R2016b/v91"

def build_satlist(): return ["ATK","SN3","JS2"]

def build_zip(tmpdir): return os.path.join(tmpdir, "zip")

def build_outputs(tmpdir): return os.path.join(tmpdir, "outputs")

def build_inputs(tmpdir): return os.path.join(tmpdir, "inputs")

def build_maskcalc(tmpdir): return os.path.join(tmpdir, "maskcalc")

def build_data(inpath): return os.path.join(inpath, "data")

def build_paths(tmpdir,appdir):
	levelpath = build_levelpath(appdir)
	resourcepath = build_resourcepath(levelpath)
	environment = build_environment()
	dirlist = []
	zippath = build_zip(tmpdir)
	outpath = build_outputs(tmpdir)
	inpath = build_inputs(tmpdir)
	maskcalc = build_maskcalc(tmpdir)
	datapath = build_data(inpath)
	for el in [zippath,outpath,inpath,maskcalc,datapath]:
		dirlist.append(el)
	#endfor
	satlist = build_satlist()
	for item in satlist:
		satpath = os.path.join(datapath,item)
		dirlist.append(satpath)
		dirlist.append(os.path.join(satpath,"L2"))
		dirlist.append(os.path.join(satpath,"tmp"))
	#endfor
	return levelpath,resourcepath,environment,dirlist,zippath,outpath,inpath,maskcalc,datapath
#enddef

def tracknofroms3url(url_s3name):
	short_s3name = url_s3name[-1:-95:-1]
	track_pass = short_s3name[20:17:-1]
	return track_pass
#enddef

def cylcefroms3url(url_s3name):
	short_s3name = url_s3name[-1:-95:-1]
	cycle = short_s3name[24:21:-1]
	return cycle
#enddef

def select_by_priority(url_list):
	mydict = dict();
	def new_dict_element(url_s3name):
		short_s3name = url_s3name[-1:-95:-1]
		track_pass = short_s3name[20:17:-1]
		cycle = short_s3name[24:21:-1]
		priority_key = short_s3name[5:3:-1]
		priority_mapping = {'NT':1,'ST':2,'NR':3,'__':4}
		if priority_key!='NT' and priority_key!='ST' and priority_key!='NR' and priority_key!='__':
			dictkey = 'error'
			priority = 5
		else:
			priority = priority_mapping[priority_key]
			dictkey = 'p' + str(track_pass) + 'c' + str(cycle)
		return dictkey,priority
	for url_s3name in url_list:
		dictkey,priority = new_dict_element(url_s3name)
		dictinfo = [priority,url_s3name]#care double brackets!
		if dictkey!='' and dictkey!='error':
			if dictkey in mydict:
				mydict[dictkey].append(dictinfo)
			else:
				mydict[dictkey] = [dictinfo]
	selected_urlname_list=[]
	for dictkey in mydict:
		val, idx = min((pair[0], idx) for (idx, pair) in enumerate(mydict[dictkey]))
		selected_urlname = mydict[dictkey][idx][1]
		selected_urlname_list.append(selected_urlname)
	return selected_urlname_list
#enddef





