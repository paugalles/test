#!/opt/anaconda/bin/python
#node to use the mask output of node A to execute the rest of the workflow
#============================IMPORTS===========================================
#standard
import sys
import os
import subprocess
import time
import zipfile
import fnmatch
import atexit
import tarfile
import ftplib

#common
import ftputil
import geojson
import utils as uutt
import urllib

#unknown
import shutil
from lxml import etree


# import the ciop functions (e.g. copy, log)
import cioppy 
ciop = cioppy.Cioppy()

#==============================================================================


for inputitem in sys.stdin:
	ciop.log('INFO',inputitem)
	inputitem = inputitem.replace('\t','')
	inputitem = inputitem.replace('\n','')
	inputitem = ciop.copy(inputitem,ciop.tmp_dir)
	f = open(inputitem,'r')
	ciop.log('INFO',f.read())





sys.exit(0)



