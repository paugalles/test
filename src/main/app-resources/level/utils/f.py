import os

def bbox2array( bbox ):
	return bbox.replace('POLYGON((','').replace('))','').replace(' ',',')

def reformat_missions( missions ):
	missions = missions.replace('S3','SN3')
	missions = missions.replace('JS2','JS') #order dependant
	missions = missions.replace('JS','JS2')
	missions = missions.replace('JN2','JS2')
	missions = missions.replace('JN','JS2')
	missions = str.split(missions,',')
	return missions

def reformat_plotname( plotname ):
	return plotname.replace(' ','_')

def reformat_mask( mask ):
	return mask.replace('&amp;','&')

def reformat_startdate( startdate ):
	return startdate.replace('-','')

def reformat_enddate( enddate ):
	return enddate.replace('-','')

def reformat_bbox( bbox ):
	#TODO non squared polygon
	if ' ' in bbox and len(bbox.split())==4:
		bbox=','.join([ bbox.split()[i] for i in [1,0,3,2] ])
	elif 'POLYGON((' in bbox:
		bbox = bbox2array( bbox )
	return bbox

def reformat_all(startdate,enddate,plotname,bbox,mask,missions):
	missions = reformat_missions( missions )
	plotname = reformat_plotname( plotname )
	mask = reformat_mask( mask )
	startdate = reformat_startdate( startdate )
	enddate = reformat_enddate( enddate )
	bbox = reformat_bbox( bbox )
	return startdate,enddate,plotname,bbox,mask,missions
#startdate,enddate,plotname,bbox,mask,missions = reformat_all(startdate,enddate,plotname,bbox,mask,missions)


def build_levelpath(appdir): return os.path.join(appdir, "level")

def build_resourcepath(levelpath): return os.path.join(levelpath, "resources")

def build_environment(): return "/opt/MCR_R2016b/v91"

def build_satlist(): return ["ATK","SN3","JS2"]

def build_zip(tmpdir): return os.path.join(tmpdir, "zip")

def build_outputs(tmpdir): return os.path.join(tmpdir, "outputs")

def build_inputs(tmpdir): return os.path.join(tmpdir, "inputs")

def build_maskcalc(tmpdir): return os.path.join(tmpdir, "maskcalc")

def build_data(inpath): return os.path.join(inpath, "data")

def build_paths(tmpdir,appdir):
	levelpath = build_levelpath(appdir)
	resourcepath = build_resourcepath(levelpath)
	environment = build_environment()
	dirlist = []
	zippath = build_zip(tmpdir)
	outpath = build_outputs(tmpdir)
	inpath = build_inputs(tmpdir)
	maskcalc = build_maskcalc(tmpdir)
	datapath = build_data(inpath)
	for el in [zippath,outpath,inpath,maskcalc,datapath]:
		dirlist.append(el)
	#endfor
	satlist = build_satlist()
	for item in satlist:
		satpath = os.path.join(datapath,item)
		dirlist.append(satpath)
		dirlist.append(os.path.join(satpath,"L2"))
		dirlist.append(os.path.join(satpath,"tmp"))
	#endfor
	return levelpath,resourcepath,environment,dirlist,zippath,outpath,inpath,maskcalc,datapath
#enddef
