#!/opt/anaconda/bin/python

#Script:
#Takes in args from application.xml
#Fetch landsat images
#Runs Filterscript, args: mission workingpath filename_B4 filename_B5 plotName bbox
#Fetches data from catalogue/FTP
#Runs lake_level_script, args: startdate enddate minpoints max_stderror plotName

#============================IMPORTS===========================================
#standard
import sys
import os
import subprocess
import time
import zipfile
import fnmatch
import atexit
import tarfile
import ftplib

#common
import ftputil
import geojson
import utils as uutt
import urllib

#unknown
import shutil
from lxml import etree


# import the ciop functions (e.g. copy, log)
import cioppy 
ciop = cioppy.Cioppy()

#==============================================================================


#Fetching all args from application.xml
startdate   = ciop.getparam("startdate") 	#format: "2010-01-01"
enddate     = ciop.getparam("enddate")		#format: "2016-01-01"
minpoints   = '1'				#format: 1
maxstderror = '1.0'				#format: 0.1
plotname    = ciop.getparam("plotname") 	#format: "kainji"
bbox	    = ciop.getparam("bbox")		#format: 4.0,10.0,5.1,11.0
mask 	    = ciop.getparam("mask")		#format: url
missions    = ciop.getparam("missions")		#format: "JS2,ATK"

startdateformat = startdate
enddateformat = enddate
startdate,enddate,plotname,bbox,mask,missions = uutt.reformat_all(startdate,enddate,plotname,bbox,mask,missions)

#==============================================================================

#setting up pathing
tmpdir = ciop.tmp_dir
appdir = ciop.application_dir
levelpath,resourcepath,environment,dirlist,zippath,outpath,inpath,maskcalc,datapath = uutt.build_paths(tmpdir,appdir)

for item in dirlist:
	os.mkdir(item)

if mask.find(','):
	masklist = mask.split(',')
else:
	masklist = [mask]
maskindex = 0

downloadedlist = []

for mask in masklist:
	time1 = time.time()	
	
	maskindex = maskindex + 1
	if len(masklist) == 1:
		indexplotname = plotname
	else:
		indexplotname = plotname + '_'+str(maskindex)
	#==============================================================================================================================================
	#obtaining mask
	#maskpath = ciop.copy(mask,inpath, extract=True)
	if 'catalog' in mask:
		maskpath = os.path.join(outpath,indexplotname+"_mask.txt")
		#tepfunctions.fetchmask(maskpath,mask,startdate)
		latlons = subprocess.Popen(["opensearch-client",mask,"wkt"],stdout=subprocess.PIPE).stdout.read().rstrip()
		latlons = latlons.replace(")","")
		latlons = latlons.split("(")
		latlons = latlons[(len(latlons)-1)]
		latlons = latlons.split(",")
		coordlist = []

		for item in latlons:
			item = item.split(" ")
			item[0] = float(item[0].rstrip())
			item[1] = float(item[1].rstrip())
			item = (item[0],item[1])
			coordlist.append(item)

		todump = geojson.FeatureCollection([geojson.Feature(geometry=geojson.Polygon(coordlist),properties={"Startdate":startdate})])
		dump = geojson.dumps(todump)

		mf = open(maskpath,'w')
		mf.write(dump)
		mf.close()
	else:
		maskpath = os.path.join(outpath,indexplotname+"_mask.tif")

		ciop.log("INFO",mask)
		quotemask = urllib.quote(mask,safe='')
		command = ['opensearch-client',mask,'enclosure']
		#command = ['opensearch-client','\"' + quotemask +'\"','enclosure']


		maskcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
		maskcatch = maskcatch.stdout.read().rstrip()
		masktmp = ciop.copy(maskcatch,outpath,extract=True)
		os.rename(masktmp,maskpath)
		
	#==============================================================================================================================================
	#obtaining all track numbers that lie within the bounding box
	bbtolerance = 0.25
	#trackdetailstr = tepfunctions.fetchtracks(bbox,bbtolerance,missions)
	searchbox = bbox
	searchbox = str.split(searchbox,',')
	searchbox = [float(i) for i in searchbox]
	searchbox[0] = searchbox[0] - bbtolerance
	searchbox[1] = searchbox[1] - bbtolerance
	searchbox[2] = searchbox[2] + bbtolerance
	searchbox[3] = searchbox[3] + bbtolerance
	searchbox = [str(i) for i in searchbox]
	searchbox = searchbox[0]+','+searchbox[1]+','+searchbox[2]+','+searchbox[3]

	trackdetailstr = list()

	for mission in missions:
		if mission == "JS2":
			source = 'https://catalog.terradue.com/jason2/series/GPS/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","JS2","L2")
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
					
		if mission == "ATK":
			source = 'https://catalog.terradue.com/saral/series/GPS/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","ATK","L2")
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
	
		if mission == "SN3":
			source = 'https://catalog.terradue.com/sentinel3/series/SR_2_LAN/search?format=atom'
			target = os.path.join(tmpdir,"inputs","data","SN3","L2")
			command = ['opensearch-client','-m','EOP','-p','count=10','-p','bbox='+searchbox,source,'track,enclosure']
		

		trackcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
		tracks = trackcatch.stdout.read().rstrip()
		tracks = str.split(tracks,'\n')
		

		for item in tracks:
			try:
				trackno,enclosure = str.split(item,',')
				#trackfile = ciop.copy(enclosure,maskcalc,extract=True)
				trackfile = ciop.copy(enclosure,target,extract=True)
				if mission == "SN3":
					SN3name = os.listdir(trackfile)[0]
					src = os.path.join(trackfile,SN3name)
					#dest = os.path.join(maskcalc,SN3name)
					dest = os.path.join(target,SN3name)
					os.rename(src,dest)
					shutil.rmtree(trackfile)
					trackfile = dest
				if os.path.exists(trackfile):
					trackdetailstr = trackdetailstr + [mission+':'+trackno+':'+trackfile]
					downloadedlist = downloadedlist + [enclosure] 
			except:
				print('No tracks found for '+mission)

	if trackdetailstr == []:
		print('No Tracks')
		sys.exit(0)
	#==============================================================================================================================================

	time2 = time.time()

	#MASKSCRIPT
	grouppath = os.path.join(tmpdir,indexplotname+"_group.txt")


	envstring = ".:"+environment+"/runtime/glnxa64:"+environment+"/bin/glnxa64:"+environment+"/sys/os/glnxa64:"+environment+"/sys/opengl/lib/glnxa64"

	os.environ["LD_LIBRARY_PATH"] = envstring
	command = os.path.join(resourcepath, "run_maskscript")


	matlab1errors = []
	for item in trackdetailstr:
		errorcatch1 = subprocess.Popen([command,grouppath,maskpath,item,bbox],stderr=subprocess.PIPE).stderr.read().rstrip()
		matlab1errors.append(errorcatch1)


	#==============================================================================================================================================

	time3 = time.time()

	#obtaining altimetry data
	#get missionpasslist from group.txt

	grouppaths = [os.path.join(tmpdir,ff) for ff in os.listdir( tmpdir ) if 'group' in ff and ff.endswith('.txt')]

	for grouppath_el in grouppaths:
		groupfile = open(grouppath_el,'r')
		grouplist = groupfile.read().rstrip()
		grouplines = grouplist.split('\n')
		grouplist = [i.split(' ') for i in grouplines]

		missionpasslist = list()
		try:
			for item in grouplist:
				missionpasslist = missionpasslist + [item[0]+':'+item[1]]
		except:
			print('No tracks cross waterbody')

		#missionpasslist = str.split(missionpasses,",")

		startvar = 'time:start='+str(startdateformat)
		endvar = 'time:end='+str(enddateformat)

		files_opbtained = open(os.path.join(outpath,"filelog.txt"),'a')

		for item in missionpasslist:
		
			sat, trackno = str.split(item,':')
			trackvar = 'track='+str(trackno)

			if sat == "JS2":
				target = os.path.join(tmpdir,"inputs","data","JS2","L2")
				source = 'https://catalog.terradue.com/jason2/series/GPS/search?format=atom'
				command = ['opensearch-client','-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']
						
			if sat == "ATK":
				target = os.path.join(tmpdir,"inputs","data","ATK","L2")
				source = 'https://catalog.terradue.com/saral/series/GPS/search?format=atom'
				command = ['opensearch-client','-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']

			if sat == "SN3":
				target = os.path.join(tmpdir,"inputs","data","SN3","tmp")
				SN3target = os.path.join(tmpdir,"inputs","data","SN3","L2")
				source = 'https://catalog.terradue.com/sentinel3/series/SR_2_LAN/search?format=atom'
				command = ['opensearch-client','-p','bbox='+searchbox,'-p',trackvar,'-p',startvar,'-p',endvar,'-p','count=unlimited',source,'enclosure']
		

			urlcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
			urllist = urlcatch.stdout.read().rstrip()
			urllist = urllist.split('\n')
			urllist = list(set(urllist) - set(downloadedlist))

			if not(urllist == ''):
				for url in urllist:
					try:
						if sat == "SN3":
							if '_NT_' in url:
								filestr = ciop.copy(url,target) #filestr will be the latest altimetry track downloaded
								SN3name = os.listdir(filestr)[0]
								src = os.path.join(filestr,SN3name)
								dest = os.path.join(SN3target,SN3name)
								os.rename(src,dest)
								shutil.rmtree(filestr)
								filestr = dest	
								files_opbtained.write(str(url)+"\n")
								files_opbtained.write(str(filestr)+"\n")
						else:
							filestr = ciop.copy(url,target) #filestr will be the latest altimetry track downloaded
							files_opbtained.write(str(url)+"\n")
							files_opbtained.write(str(filestr)+"\n")
						downloadedlist = downloadedlist + [url]
					except:
						print('error copying file')

			#		tounzip = os.path.join(zippath, filestr)
			#		with zipfile.ZipFile(tounzip,'r') as zip_ref:
			#			filestr = zip_ref.namelist()(0)
			#			zip_ref.extractall(target)

		files_opbtained.close()

	#==============================================================================================================================================

	#LAKE_LEVEL_SCRIPT

	time4 = time.time()

	for grouppath_el in grouppaths:
		indexplotname_el = os.path.split(grouppath_el)[1].replace('.txt','').replace('group','')
		command = os.path.join(resourcepath, "run_lake_level_wrapper")
		#multiplot = '1'
		fullcatch = subprocess.Popen([command,tmpdir+"/",grouppath_el,startdate,enddate,minpoints,maxstderror,indexplotname_el],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
		errorcatch2 = fullcatch.stderr.read().rstrip()
		outcatch = fullcatch.stdout.read().rstrip()

	#==============================================================================================================================================

	time5 = time.time()


	#testing stuff

	down1time = time2-time1
	matlab1time = time3-time2
	down2time = time4-time3
	matlab2time = time5-time4

	file_timings = open(os.path.join(outpath,indexplotname+"_timelog.txt"),'w')
	file_timings.write("Download time: "+str(down1time)+"\nMatlab part 1 time: "+str(matlab1time)+"\nDownload time2: "+str(down2time)+"\nMatlab part 2 time: "+str(matlab2time)+"\n\n")
	file_timings.close()


	file_object = open(os.path.join(outpath,indexplotname+"_test.txt"),'w')

	for item in matlab1errors:
		file_object.write("\n"+str(item)+" \n")
	file_object.write(str(errorcatch2)+" \n\n")
	file_object.write(str(outcatch)+" \n\n")

	for item in trackdetailstr:
		file_object.write(str(item)+'\n')

	file_object.close()

	file_group = open(os.path.join(outpath,indexplotname+"_group.txt"),'w')
	for item in grouplines:
		file_group.write(str(item)+"\n")

	file_group.close()

	#experiment: generate a world file for the jpgs
	#filelist = []
	#dirlist = os.listdir(outpath)
	#for item in dirlist:
	#	if os.path.isfile(os.path.join(outpath,item)):
	#		filelist.append(os.path.join(outpath,item))
	#
	#for item in filelist:
	#	if item.endswith('.jpg'):
	#		worldfile = open(item+"w",'w')
	#		worldfile.write('0.06 \n 0.0 \n 0.0 \n -0.01 \n -170.0 \n 80.0')
	#		worldfile.close()

	#zip the jpgs
	filelist = []
	dirlist = os.listdir(outpath)
	for item in dirlist:
		if os.path.isfile(os.path.join(outpath,item)):
			filelist.append(os.path.join(outpath,item))
	
	for item in filelist:
		if item.endswith('.jpg'):
			zf = zipfile.ZipFile(item+'.zip',mode='w')
			try:
				zf.write(item,os.path.basename(item))
			finally:
				zf.close()
				os.remove(item)
			
			

#note that the publish function publishes the outputs for all lakes and thus it needs to be outside of the loop
#ciop.publish(outpath,recursive=True,metalink=True)
ciop.publish(tmpdir,recursive=True,metalink=True)

sys.exit(0)






