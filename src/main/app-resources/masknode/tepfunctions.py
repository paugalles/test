#The functions required for the tep run.py 
import sys
import atexit
import os
import os.path
import subprocess
from lxml import etree
from subprocess import call
import tarfile
import ftplib
import ftputil,ftputil.session
import fnmatch
import zipfile
from geojson import Feature, Polygon, FeatureCollection
import geojson
import time
import urllib
import shutil

def fetchmask(maskpath,mask,startdate):
	latlons = subprocess.Popen(["opensearch-client",mask,"wkt"],stdout=subprocess.PIPE).stdout.read().rstrip()
	latlons = latlons.replace(")","")
	latlons = latlons.split("(")
	latlons = latlons[(len(latlons)-1)]
	latlons = latlons.split(",")
	coordlist = []

	for item in latlons:
		item = item.split(" ")
		item[0] = float(item[0].rstrip())
		item[1] = float(item[1].rstrip())
		item = (item[0],item[1])
		coordlist.append(item)

	todump = FeatureCollection([Feature(geometry=Polygon(coordlist),properties={"Startdate":startdate})])
	dump = geojson.dumps(todump)

	mf = open(maskpath,'w')
	mf.write(dump)
	mf.close()
	return


def fetchtracks(bbox,bbtolerance,missions):
	#in case we need to modify the bounding box
	searchbox = bbox
	searchbox = str.split(searchbox,',')
	searchbox = [float(i) for i in searchbox]
	searchbox[0] = searchbox[0] - bbtolerance
	searchbox[1] = searchbox[1] - bbtolerance
	searchbox[2] = searchbox[2] + bbtolerance
	searchbox[3] = searchbox[3] + bbtolerance
	searchbox = [str(i) for i in searchbox]
	searchbox = searchbox[0]+','+searchbox[1]+','+searchbox[2]+','+searchbox[3]

	trackdetailstr = list()

	for mission in missions:
		if mission == "JS2":
			source = 'https://catalog.terradue.com/jason2/series/GPS/search?format=atom'
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
					
		if mission == "ATK":
			source = 'https://catalog.terradue.com/saral/series/GPS/search?format=atom'
			command = ['opensearch-client','-m','EOP','-p','cycle=020','-p','bbox='+searchbox,source,'track,enclosure']
	
		if mission == "SN3":
			source = 'https://catalog.terradue.com/sentinel3/series/SR_2_LAN/search?format=atom'
			command = ['opensearch-client','-m','EOP','-p','count=10','-p','bbox='+searchbox,source,'track,enclosure']
		

		trackcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
		tracks = trackcatch.stdout.read().rstrip()
		tracks = str.split(tracks,'\n')
	

		for item in tracks:
			try:
				trackno,enclosure = str.split(item,',')
				trackfile = ciop.copy(enclosure,maskcalc,extract=True)
				if mission == "SN3":
					SN3name = os.listdir(trackfile)[0]
					src = os.path.join(trackfile,SN3name)
					dest = os.path.join(maskcalc,SN3name)
					os.rename(src,dest)
					shutil.rmtree(trackfile)
					trackfile = dest
				if os.path.exists(trackfile):
					trackdetailstr = trackdetailstr + [mission+':'+trackno+':'+trackfile]
			except:
				print('No tracks found for '+mission)

	if trackdetailstr == []:
		print('No Tracks')
		sys.exit(0)

	return trackdetailstr
	

