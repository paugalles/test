#!/opt/anaconda/bin/python
#a short test script
#============================IMPORTS===========================================
#standard
import sys
import os
import subprocess
import time
import zipfile
import fnmatch
import atexit
import tarfile
import ftplib

#common
import ftputil
import geojson
import utils as uutt
import urllib
import urlparse

#unknown
import shutil
from lxml import etree

# import the ciop functions (e.g. copy, log)
import cioppy 
ciop = cioppy.Cioppy()

outpath = ciop.tmp_dir

mask = ciop.getparam("mask")

if mask.find(','):
	masklist = mask.split(',')
else:
	masklist = [mask]
maskindex = 0



myurl1 = 'https://recast.terradue.com/t2api/search/paugalles/_results/workflows/hydrology_tep_dcs_small_water_body_mapping_small_water_bodies_2_0_8/run/94194dfa-f6bb-11e7-947b-0242ac110007/0000600-170920182652224-oozie-oozi-W?id=cfc5eb5e1356f5a14144bdd5d44d702e6349404d'
#myurl2 = 'https://store.terradue.com/paugalles/_results/workflows/hydrology_tep_dcs_small_water_body_mapping_small_water_bodies_2_0_8/run/94194dfa-f6bb-11e7-947b-0242ac110007/0000600-170920182652224-oozie-oozi-W/20171119T000000_20171121T000000_water_mask_Surf.tif'


import urllib
f = urllib.urlopen(myurl1)
print f.read()

print '************************'
#aux = ciop.casmeta("eop:orbitDirection", myurl1)
aux = ciop.casmeta("productType", myurl1)
print str(type(aux))
print aux
aux = ciop.casmeta("features:properties", myurl1)
print aux
aux = ciop.casmeta("features:properties:links", myurl1)
print aux
aux = ciop.casmeta("features:properties:links:@type", myurl1)
print aux
#aux = ciop.casmeta("eop:orbitDirection", myurl2)
#print aux
print '************************'


'''
features
productType
'''



'''
#maskfilelist = []

#==============================================================================================================================================
#obtaining masks
for mask in masklist:
	time1 = time.time()
	
	maskindex = maskindex + 1
	#indexplotname = str(maskindex)
	parsed = urlparse.urlparse(mask)
	
	if not mask.startswith('https://recast.'):
		#not mask.endswith('.tif')
		indexplotname = urlparse.parse_qs(parsed.query)['uid'][0]
		maskpath = os.path.join(outpath,indexplotname+".txt")
		#tepfunctions.fetchmask(maskpath,mask,startdate)
		latlons = subprocess.Popen(["opensearch-client",mask,"wkt"],stdout=subprocess.PIPE).stdout.read().rstrip()
		latlons = latlons.split('),(')
		latlons = [''.join([subel for subel in el if subel not in '()ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz']) for el in latlons]
		latlons = [el for el in latlons if el!='']
		latlons = latlons[0]#TODO, check for holes/islands
		latlons = latlons.split(",")
		coordlist = []

		for item in latlons:
			item = item.split(" ")
			item[0] = float(item[0].rstrip())
			item[1] = float(item[1].rstrip())
			item = (item[0],item[1])
			coordlist.append(item)

		todump = geojson.FeatureCollection([geojson.Feature(geometry=geojson.Polygon(coordlist),properties={"property":'string'})])
		dump = geojson.dumps(todump)
		mf = open(maskpath,'w')
		mf.write(dump)
		mf.close()
	else:
		indexplotname = urlparse.parse_qs(parsed.query)['id'][0].replace('.tif','')
		maskpath = os.path.join(outpath,indexplotname+".tif")
		ciop.log("INFO",mask)
		quotemask = urllib.quote(mask,safe='')
		command = ['opensearch-client',mask.replace('.tif',''),'enclosure']
		#command = ['opensearch-client','\"' + quotemask +'\"','enclosure']
		maskcatch = subprocess.Popen(command,stdout=subprocess.PIPE)
		maskcatch = maskcatch.stdout.read().rstrip()
		masktmp = ciop.copy(maskcatch,outpath,extract=True)
		masktmp = masktmp.replace('.tif','')+'.tif'
		print maskcatch
		print masktmp
		os.rename(masktmp,maskpath)

	#maskfilelist.append(maskpath)
	time2 = time.time()
	ciop.log('INFO',"time taken: "+str(time2-time1))
	ciop.log('INFO',maskpath)
	ciop.publish(maskpath)

#for filepath in maskfilelist:		
#	ciop.publish(filepath,mode='silent')

sys.exit(0)









'''









